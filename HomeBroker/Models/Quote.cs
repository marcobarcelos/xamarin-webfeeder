﻿using System;
using Newtonsoft.Json;

namespace HomeBroker.Models
{
	public class Quote
	{
		public string Code { get; set; }

		[JsonProperty("2")]
		public float? LastPrice { get; set; }

		[JsonProperty("21")]
		public float? Variation { get; set; }
	}
}
