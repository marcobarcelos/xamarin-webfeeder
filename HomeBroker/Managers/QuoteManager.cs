﻿using System;
using System.Collections.Generic;
using HomeBroker.Models;

namespace HomeBroker.Managers
{
	public class QuoteManager
	{
		private readonly Dictionary<string, Quote> quotes;

		public QuoteManager()
		{
			quotes = new Dictionary<string, Quote>();
		}

		public Quote UpdateQuote(Quote quote)
		{
			if (quotes.ContainsKey(quote.Code))
			{
				var storedQuote = quotes[quote.Code];

				if (quote.LastPrice.HasValue)
					storedQuote.LastPrice = quote.LastPrice;

				if (quote.Variation.HasValue)
					storedQuote.Variation = quote.Variation;

				return storedQuote;
			}
			else
			{
				quotes.Add(quote.Code, quote);
				return quote;
			}
		}
	}
}
