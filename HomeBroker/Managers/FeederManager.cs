﻿using System;
using System.Collections.Generic;
using HomeBroker.Messages;
using HomeBroker.Models;

namespace HomeBroker.Managers
{
	public static class FeederManager
	{
		public readonly static List<string> DefaultFilterList = new List<string>() { "2", "21" };
		private const string FeederEndpoint = "ws://webfeedermock.marcobarcelos.com";
		private const string FeederUserName = "username";
		private const string FeederPassword = "password";
		private const string QuoteMessageType = "QuoteType";
		private const string FilterSeparator = ",";
		private static string authenticationToken;

		private static readonly WebSocket webSocket;
		private static QuoteManager quoteManager;

		public delegate void QuoteUpdatedHandler(Quote quote);
		public static event QuoteUpdatedHandler QuoteUpdated;

		static FeederManager()
		{
			webSocket = new WebSocket(FeederEndpoint);
			quoteManager = new QuoteManager();

			webSocket.Connected += WebSocket_Connect;
			webSocket.MessageReceived += WebSocket_MessageReceived;
		}

		public static void Connect()
		{
			webSocket.Connect();
		}

		static void WebSocket_Connect(object sender, EventArgs e)
		{
			webSocket.Send(new AuthenticationMessage(FeederUserName, FeederPassword));
		}

		static void WebSocket_MessageReceived(Newtonsoft.Json.Linq.JObject obj)
		{
			if (obj["token"] != null)
			{
				authenticationToken = (string)obj["token"];
			}
			else
			{
				var type = (string)obj["type"];

				if (type != null)
				{
					if (type == QuoteMessageType)
					{
						var response = obj.ToObject<SubscribeQuoteResponse>();

						// Adds quote code to response object
						response.Quote.Code = response.Parameter.ToUpper();

						var quote = quoteManager.UpdateQuote(response.Quote);

						if (QuoteUpdated != null) {
							QuoteUpdated(quote);
						}
					}
				}
			}
		}

		public static void SubscribeQuote(string quoteCode, List<string> filterList = null)
		{
			var filter = string.Join(FilterSeparator, filterList ?? DefaultFilterList);
			var subscriptionRequestMessage = new SubscribeQuoteMessage(quoteCode, filter, isSubscribe: true) {
				Token = authenticationToken
			};

			webSocket.Send(subscriptionRequestMessage);
		}
	}
}
