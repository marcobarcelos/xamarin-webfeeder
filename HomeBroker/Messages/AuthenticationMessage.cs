﻿using System;

namespace HomeBroker.Messages
{
	public class AuthenticationMessage : BaseMessage
	{
		private const string LoginModule = "login";
		private const string AuthenticationService = "authentication";

		public AuthenticationMessage(string login, string password)
			: base(LoginModule, AuthenticationService)
		{
			this.Parameters = new AuthenticationParameter()
			{
				Login = login,
				Password = password
			};
		}
	}
}
