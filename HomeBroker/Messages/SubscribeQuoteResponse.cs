﻿using System;
using HomeBroker.Models;
using Newtonsoft.Json;

namespace HomeBroker.Messages
{
	public class SubscribeQuoteResponse
	{
		public string Type { get; set; }

		public string Parameter { get; set; }

		[JsonProperty("values")]
		public Quote Quote { get; set; }
	}
}
