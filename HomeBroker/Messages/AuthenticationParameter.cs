﻿using System;

namespace HomeBroker.Messages
{
	public class AuthenticationParameter : IMessageParameter
	{
		public string Login { get; set; }

		public string Password { get; set; }
	}
}
