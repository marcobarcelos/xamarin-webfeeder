﻿using System;
using Newtonsoft.Json;

namespace HomeBroker.Messages
{
	public class SubscribeQuoteParameter : IMessageParameter
	{
		[JsonProperty("subsbribetype")]
		public int SubscribeType { get; set; }

		public string Filter { get; set; }
	}
}
