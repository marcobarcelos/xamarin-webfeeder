﻿using System;

namespace HomeBroker.Messages
{
	public abstract class BaseMessage
	{
		public string Module { get; set; }

		public string Service { get; set; }

		public IMessageParameter Parameters { get; set; }

		public BaseMessage()
		{

		}

		public BaseMessage(string module, string service)
		{
			Module = module;
			Service = service;
		}
	}
}
