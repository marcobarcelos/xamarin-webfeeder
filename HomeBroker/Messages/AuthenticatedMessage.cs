﻿using System;

namespace HomeBroker.Messages
{
	public abstract class AuthenticatedMessage : BaseMessage
	{
		public string Token { get; set; }

		public AuthenticatedMessage() : base()
		{

		}

		public AuthenticatedMessage(string module, string service)
			: base(module, service)
		{

		}
	}
}
