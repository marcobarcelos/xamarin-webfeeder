﻿using System;
using Newtonsoft.Json;

namespace HomeBroker.Messages
{
	public class SubscribeQuoteMessage : AuthenticatedMessage
	{
		private const string QuotesModule = "quotes";
		private const string QuoteService = "quote";

		[JsonProperty("parameterGet")]
		public string QuoteCode { get; set; }

		public SubscribeQuoteMessage(string quoteCode, string filter, bool isSubscribe)
			: base(QuotesModule, QuoteService)
		{
			this.QuoteCode = quoteCode;
			this.Parameters = new SubscribeQuoteParameter()
			{
				Filter = filter,
				SubscribeType = Convert.ToInt32(isSubscribe)
			};
		}
	}
}
