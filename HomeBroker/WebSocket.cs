﻿using System;
using Websockets;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace HomeBroker
{
	public class WebSocket
	{
		private IWebSocketConnection connection;
		private string endpoint;
		private readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings() {
			ContractResolver = new CamelCasePropertyNamesContractResolver()
		};

		public delegate void ErrorReceivedHandler(string message);
		public delegate void MessageReceivedHandler(JObject obj);

		public event EventHandler Disconnected;
		public event EventHandler Connected;
		public event MessageReceivedHandler MessageReceived;
		public event ErrorReceivedHandler ErrorReceived;

		public WebSocket(string endpoint)
		{
			this.endpoint = endpoint;

			connection = WebSocketFactory.Create();

			connection.OnMessage += Connection_OnMessage;
			connection.OnError += Connection_OnError;
			connection.OnOpened += Connection_OnOpened;
			connection.OnClosed += Connection_OnClosed;
		}

		public void Connect()
		{
			connection.Open(endpoint);
		}

		public void Disconnect()
		{
			connection.Close();
		}

		void Connection_OnOpened()
		{
			Debug.WriteLine("WebSocket connected!");

			if (Connected != null) {
				Connected(this, null);
			}
		}

		void Connection_OnMessage(string obj)
		{
			var jObject = JObject.Parse(obj);
			Debug.WriteLine("Received message: {0}", obj);

			if (MessageReceived != null) {
				MessageReceived(jObject);
			}
		}

		void Connection_OnError(string obj)
		{
			Debug.WriteLine("Error occurred: {0}", obj);

			if (ErrorReceived != null) {
				ErrorReceived(obj);
			}
		}

		void Connection_OnClosed()
		{
			Debug.WriteLine("Connection closed");

			if (Disconnected != null) {
				Disconnected(this, null);
			}
		}

		public void Send(object data)
		{
			string json = JsonConvert.SerializeObject(data, jsonSettings);
			Send(json);
		}

		public void Send(string data)
		{
			Debug.WriteLine("Sending message: {0}", data);
			connection.Send(data);
		}
	}
}
