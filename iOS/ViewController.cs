﻿using System;
using HomeBroker.Managers;
using UIKit;

namespace HomeBroker.iOS
{
	public partial class ViewController : UIViewController
	{
		int count = 1;

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			FeederManager.QuoteUpdated += (quote) => {
				InvokeOnMainThread(() => {
					var title = string.Format("{0} - {1} - {2}", quote.Code, quote.LastPrice, quote.Variation);
					Button.SetTitle(title, UIControlState.Normal);
				});
			};

			FeederManager.Connect();

			// Perform any additional setup after loading the view, typically from a nib.
			Button.AccessibilityIdentifier = "myButton";
			Button.TouchUpInside += delegate {

				FeederManager.SubscribeQuote("petr4");
			};
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.		
		}
	}
}
