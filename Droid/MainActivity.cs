﻿using Android.App;
using Android.Widget;
using Android.OS;
using HomeBroker.Models;
using HomeBroker.Managers;

namespace HomeBroker.Droid
{
	[Activity(Label = "HomeBroker", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			Button button = FindViewById<Button>(Resource.Id.myButton);

			Websockets.Droid.WebsocketConnection.Link();


			FeederManager.QuoteUpdated += (quote) => {
				RunOnUiThread(() => {
					button.Text = string.Format("{0} - {1} - {2}", quote.Code, quote.LastPrice, quote.Variation);
				});
			};

			FeederManager.Connect();

			// Get our button from the layout resource,
			// and attach an event to it


			button.Click += delegate {
				FeederManager.SubscribeQuote("petr4");
			};
		}
	}
}

